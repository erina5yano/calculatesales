package jp.alhinc.yano_erina.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class Calculatesales {
	public static void main(String[] args) {
		if(args.length!=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		Map<String, String> branchName = new HashMap<String, String>();
		Map<String,Long> branchSale = new HashMap<String,Long>();

		List<String> files = new ArrayList<>();

		}
		BufferedReader br = null;
		try {

			File file = new File(args[0],"branch.lst");
			if (!file.exists()) {//支店定義ファイルの存在を確認←！をつけて存在しない場合
	            System.out.println("支店定義ファイルが存在しません");
	            return;//支店定義ファイルが存在しない場合
			}


			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);



			String line;
			while((line =br.readLine()) !=null) {
				String[] store=line.split(",",-1);
				if((store.length != 2)||(!store[0].matches("^[0-9]{3}$"))){
					System.out.print("支店定義ファイルのフォーマットが不正です");
					return;
				}

				branchName.put(store[0],store[1]);
				branchSale.put(store[0], 0L);
			}
		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
		}finally {
			try {
			if(br !=null) {
					br.close();
			}
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				return;
				}
			}

		File []files = new File(args[0]).listFiles();
        List<File> rcdFiles = new ArrayList<>();
        for(int i =0; i < files.length; i++) {

             //取得した一覧を表示する
            if (list[i].getName().matches("[0-9]{8}.+rcd$")) {
                files.add(list[i].getName());
            }
        }
        for(int i = 0; i <files.size(); i++) {
        	try {
        		File file =new File(args[0],files.get(i));
        		FileReader fr =new FileReader(file);
        		br =new BufferedReader(fr);
        		String line;
        		List<String> value = new ArrayList<>();
	            while((line =br.readLine()) !=null) {
	            	value.add(line);
	            }
	           Long sale = branchSale.get(value.get(0));//支店コードと売り上げを取った
	            //支店コード＋売り上げをしたい
	           Long totalSale = sale + Long.parseLong(value.get(1));
	           branchSale.put(value.get(0),totalSale);
			//売上げファイルを読み込み、支店コード、売上額を抽出する。9月11日ここまで終了。
			//抽出した売上額を該当する支店の合計金額にそれぞれ加算する。それを売上ファイルの数だけ繰り返す。明日やる。
        	}catch(IOException e) {
    			System.out.println("エラーが発生しました。");
    		}finally {
    			if(br !=null) {
    				try {
    					br.close();
    				} catch(IOException e) {
    					System.out.println("closeできませんでした。");
    				}
    			}
    		}
        }
        BufferedWriter bw = null;
       try {
    	   File outfile = new File(args[0],"branch.out");
         	FileWriter fw = new FileWriter(outfile);
         	bw = new BufferedWriter(fw);

         	 for (String key : branchSale.keySet()) {
                 bw.write(key + "," + branchName.get(key) + "," + branchSale.get(key));
                 bw.newLine();
         	 }



         	 }catch(IOException e) {
         		 System.out.println("エラーが発生しました。");
         	 }finally {
         		 if(br !=null) {
         			 try {
         				 bw.close();
         			 } catch(IOException e) {
         				 System.out.println("closeできませんでした。");

         			 }
         		 }
         	 }
		}
	}




